def adjacent_in_range(elt, range)
  arr = range.to_a
  index = arr.index(elt)
  high = arr.length - 1 
  if (1..high - 1).include? index
    indices = [index - 1, index + 1]
  else
    indices = [(index == 0) ? 1 : high - 1]
  end
  indices.collect { |i| arr[i] }
end

class KeyOffBoardError < KeyError
end

class Tile
  attr_accessor :key
  attr_reader :letter, :number

  def initialize(_key)
    Tile.valid_key? _key or raise KeyOffBoardError      
    @key = _key
    @letter = _key[0]
    @number = _key[1]
  end

  def self.valid_key? (key)
    (('A'..'I').include? key[0]) &&
      ( (1..11).include? key[1].to_i) &&
      2 == key.length
  end

  def letter_neighbors
    adjacent_in_range(@letter, ('A'..'I'))
  end

  def number_neighbors
    adjacent_in_range(@number, (1..11))
  end
  
  def neighbors
    @_neighbors ||=
      letter_neighbors.collect { |l|  [ l, @number] }
        .concat number_neighbors.collect { |n| [@letter, n] }
  end
end


class Board
  attr_accessor :grid
  
  def initialize
    _grid = {}
    letters = %w{ A B C D E F G H I }
    numbers = (1..11)
    letters.each do |letter|
      numbers.each do  |number|
        _grid = _grid.merge({ [letter, number] => 0 })
      end
    end
    self.grid= _grid
  end

  def [] (key)
    self.grid[key]
  end

  def []=(key,value)
    self.grid[key] = value
  end
end


