require 'rspec'
require_relative '../acquire'

# we want tiles with letters A to I and numbers 1 to 11 on a 121 tile board 

describe 'adjacent_in_range' do
  context 'interior values' do
    it 'yields two values' do
      expect(adjacent_in_range(5, (1..10))).to eq [4,6]
    end
  end
  context 'extremal values' do
    it 'gives the second item when called with first item' do
      expect(adjacent_in_range(1,(1..10))).to eq [2]
    end
    it 'gives the second to last item when called with the last item' do
      expect(adjacent_in_range(10,(1..10))).to eq [9]
    end
  end
end

describe 'TileClass' do
  context 'checking valid ranges' do
    it 'allows A1' do
      expect(Tile.valid_key? ['A', 1]).to be true
    end
    it 'does not allow A12' do
      expect(Tile.valid_key? ['A', 12] ).to be false
    end
    it 'does not allow 2 1' do
      expect(Tile.valid_key? [2, 1] ).to be false
    end
    it 'allows I11' do
      expect(Tile.valid_key? ['I', 11]).to be true
    end

    it 'does not allow AB' do
      expect(Tile.valid_key? ['A', 'B']).to be false
    end
  end
end

describe 'Tile' do
  context 'with valid key' do
    before do
      @key = ['C', 5]
      @tile = Tile.new @key
    end
    
    it 'can be created' do
      expect(@tile.class).to be Tile
    end
    
    it 'can retrieve its key' do
      expect(@tile.key).to equal @key
    end

    context 'finding its neighbors' do
      it 'can find neighbors' do
        expect(@tile.neighbors.length).to eq 4
      end

      it 'finds adjacent locations' do
        expect(@tile.neighbors).to include ['C', 6]
        expect(@tile.neighbors).to include ['D', 5]
      end
      it 'excludes diagonal locations' do
        expect(@tile.neighbors).not_to include ['D', 6]
      end

      it 'knows nothing before A' do
        expect(Tile.new(['A', 1]).letter_neighbors).to eq ['B']
      end
      it 'knows nothing after I' do
        expect(Tile.new(['I', 1]).letter_neighbors).to eq ['H']
      end
      it 'knows nothing before 1' do
        expect(Tile.new(['A', 1]).number_neighbors).to eq [2]
      end
      it 'knows nothing after 11' do
        expect(Tile.new(['A', 11]).number_neighbors).to eq [10]
      end

      # should add test here to verify letter_neighbors only called once when neighbors
      # called twice
      
    end
  end
  
  context 'with invalid key' do
    before do
      @key = ['J', 12]
    end

    it 'cannot be created' do
      expect { Tile.new @key} .to raise_error( KeyOffBoardError)
    end
  end
end
